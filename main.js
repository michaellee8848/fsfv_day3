/**
 * Created by Michael on 2016/6/29.
 */

var express = require("express");
var app = express ();

//process request, usually from specific to general request

//app.use (middleware)
//examine behaviour of /time
app.use(function (req, res, next) {
    var currTime = new Date();
    res.send ("<h1> The current time is:" + currTime + "</h1>");
    next();
});

app.get("/time", function(req, res) {
    var currTime = new Date();
    //set the status code
    res.status(200);
    //set the content type
    res.type('text/html');
    //custom headers
    res.set("My-X-Header", "hello");
    //send the result
    //the <h1> tage can be used in js as well, since browser is forgiven and take the html syntax as well.
    res.send ("<h1> The current time is:" + currTime + "</h1>");
});


app.get("/image", function(req, res) {
    var maps = ["map1.gif", "map2.gif", "map3.jpg", "map4.jpg"];
    //set the status code
    res.status(202);
    //set the content type
    res.type("text/html");
    //send the result
    //randomly pic a number, get a fraction from "Math.random()", the fraction * array length get the
    // random number of the array.
    var idx = Math.floor(Math.random() * maps.length);
    console.log(idx)
    // res.send("<img src='image/map4.jpg'>");
    res.send("<img src = 'image/" + maps[idx] + "'>"
         + "<h2>" + maps[idx] + "</h2>");
//    can not use "send" twice, use once only
});

app.use(function (req, res, next) {
    console.info("Incoming request: %s", req.originalUrl);
    next();
});

var expressStatic = express.static(__dirname + "/public");
console.info(">> %s", (typeof expressStatic));

app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname + "/bower_components"));

app.use(function(req, res) {
    console.info("File not found in public: %s", req.originalUrl);
    res.redirect("/error.html");
});

app.set ("port",
    process.argv[2] || process.env.APP_PORT || 3000);

app.listen(app.get("port"), function() {
    console.info ("Application started on port %d", app.get("port"));
});